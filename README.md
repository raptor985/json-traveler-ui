# JSON Traveler UI

To debug, use:
```
npm run start
```
By default, in debug mode, requests to /backend route are forwarded to 127.0.0.1:8080.


To build:
```
npm run build
```