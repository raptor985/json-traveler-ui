import React, {Reducer} from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import {Provider} from 'react-redux'
import {applyMiddleware, combineReducers, createStore} from 'redux'
import reducers from '../redux/reducers'
import {IS_DEBUG} from "./constants";
import reduxLogger from "redux-logger";
import reduxThunk from "redux-thunk";

const rootReducer = combineReducers(reducers);

const store = createStore(rootReducer as Reducer<any, any>, applyMiddleware(
    reduxThunk,
    ...(IS_DEBUG ? [reduxLogger] : [])
));

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
