import React, {Suspense} from 'react';

import {BrowserRouter, Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import Select from "../pages/Select";
import Results from "../pages/Results";
import Alerts from "../chunks/Alerts";

type Props = StateProps;

class App extends React.PureComponent<Props, {}> {
    render() {
        return (
            <div className="App">
                <div className="container">
                    <Suspense fallback="loading">
                        <BrowserRouter>
                            <Switch>
                                <Route path="/results">
                                    <Results />
                                </Route>
                                <Route path="/">
                                    <Select />
                                </Route>
                            </Switch>
                        </BrowserRouter>
                    </Suspense>
                    <Alerts />
                </div>
            </div>
        );
    }
}

interface StateProps {
}

const mapState = (state: any) => ({
})

export default connect<StateProps, {}, {}>(mapState, () => ({}))(App);