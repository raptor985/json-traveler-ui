declare var NODE_ENV: string;

export const IS_DEBUG = (process.env.NODE_ENV === 'development');
export const APP_NAME = 'JSON Scanner';
export const BACKEND_URL = '/backend';

export const DEFAULT_JSONPATH = '$.products.*[?(\'web\' in @.tags && @.forSale == true)]';
export const DEFAULT_FILE_MASK = 'example*.json';