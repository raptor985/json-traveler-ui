import jsonQuery from "./jsonQuery";
import alerts from "./alerts";

export default {
    jsonQuery,
    alerts,
}