import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {Dispatch} from "react";

let idSequence = 1

enum AlertLevel {
    ERROR
}

export interface Alert {
    level: AlertLevel,
    message: string
}

export type AlertsState = {[id: string]: Alert}

const EMPTY_STATE: AlertsState = {}

const slice = createSlice({
    name: 'alert',
    initialState: EMPTY_STATE,
    reducers: {
        added: (state: AlertsState, action: PayloadAction<Alert>) => {
            return {...state, [idSequence++]: action.payload}
        },
        closed: (state: AlertsState, action: PayloadAction<string>) => {
            const newState = {...state};
            delete newState[action.payload];
            return newState;
        }
    }
});

export const errorAlert = (message: string) => (dispatch: Dispatch<any>) => {
    dispatch(slice.actions.added({level: AlertLevel.ERROR, message}));
}

export const closeAlert = (id: string) => (dispatch: Dispatch<any>) => {
    dispatch(slice.actions.closed(id));
}

export default slice.reducer;