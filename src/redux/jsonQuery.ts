import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {Dispatch} from "react";
import {apiGet} from "../util/exchange/apiActions";
import {urlWithParams} from "../util/url";
import {errorAlert} from "./alerts";


export enum JsonQueryStatus {
    PENDING,
    LOADED,
    FAILED
}

export interface JsonQueryState {
    status: JsonQueryStatus,
    data: {[filename: string]: any}
}

interface JsonQueryResponse {
    data: {[filename: string]: any}
}

const EMPTY_STATE: JsonQueryState = {
    status: JsonQueryStatus.PENDING,
    data: {}
};

const slice = createSlice({
    name: 'jsonQuery',
    initialState: EMPTY_STATE,
    reducers: {
        pending: () => ({...EMPTY_STATE}),
        querySkipped: () => ({...EMPTY_STATE, status: JsonQueryStatus.LOADED}),
        loaded: (state: JsonQueryState, action: PayloadAction<JsonQueryResponse>) => {
            return {...state, data: action.payload, status: JsonQueryStatus.LOADED}
        },
        //TODO: Error state
    }
});

export const selectJson = (files: string, query: string) => (dispatch: Dispatch<any>) => {
    if (!files || !query) {
        dispatch(slice.actions.querySkipped());
        return
    }
    apiGet(urlWithParams('/jscan', {files, query}), {
        success: {
            callback: (dispatch: any, response: any) => {
                dispatch(slice.actions.loaded(response));
            }
        },
        error: {
            callback: (dispatch: any, code: number, message: string) => {
                dispatch(slice.actions.querySkipped());
                errorAlert('Can\'t retrieve JSON data: ' + message)(dispatch);
            }
        }
    })(dispatch);
}

export const resetJson = () => (dispatch: Dispatch<any>) => dispatch(slice.actions.pending())

export default slice.reducer;