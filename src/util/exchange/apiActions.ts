import {Dispatch} from "react";
import {apiHttpGet} from "./apiHttpActions";

interface ApiCallParams {
    success?: {
        callback?: (dispatch: Dispatch<any>, data: any) => void,
    },
    error?: {
        callback?: (dispatch: Dispatch<any>, code: number, mesage: string) => void,
    }
}

export const apiGet = (path: string, params: ApiCallParams) => (dispatch: Dispatch<any>) => {
    const cleanPath = path.startsWith("/") ? path.substr(1, path.length) : path;
    const successCallback = params?.success?.callback
    const errorCallback = params?.error?.callback
    apiHttpGet(cleanPath)({
        success: {
            callback: successCallback
        },
        error: {
            callback: errorCallback
        }
    })(dispatch)
}