import axios from "axios/index";
import {BACKEND_URL, IS_DEBUG} from "../../common/constants";

const BAD_REQUEST = 'BAD_REQUEST'   //TODO: Assign to reducer
const ERROR_RESPONSE = 'ERROR_RESPONSE'  //TODO: Assign to reducer

//TODO: Refactor to TS with generic api action handling

const genericHttpApiAction = (method, url, requestBody, params = {}) => dispatch => {
    const performFinal = () => {
        if (!params.end) {
            return;
        }
        const {
            callback,           //коллбэк, который нужно вызвать по итогу действия
        } = params.end;
        if (callback) {
            callback(dispatch);
        }
    };

    const started = performance.now();

    axios({
        method: method,
        url: `${BACKEND_URL}/${url}`,
        data: requestBody
    }).then(response => {
        if (IS_DEBUG) { // TODO: This logic should be entirely excluded from production build
            const requestNanos = performance.now() - started
            console.debug("HTTP request to " + url + " took " + Math.round(requestNanos) + " ms.")
        }

        if (!params.success) {
            return;
        }
        const {
            type,               //тип действия, которое диспатчить при успехе
            customPayload,      //замена пэйлоада, если нам он не важен
            payloadAugment,     //данные, которые нужно добавить к ответу сервера, в пэйлоад
            callback,           //коллбэк, который нужно вызвать по итогу действия
        } = params.success;
        if (type) {
            dispatch({
                type,
                payload: customPayload ? customPayload : Object.assign(response.data, payloadAugment)
            });
        }
        if (callback) {
            callback(dispatch, response.data.data, response);
        }
        performFinal();
    }).catch(error => {
        const {ignore, callback} = params.error || {};
        if (ignore) {
            console.error(error)
        } else if (error.response && error.response.status === 400) {
            dispatch({
                type: BAD_REQUEST,
                payload: error.response.data
            })
        } else {
            dispatch({
                type: ERROR_RESPONSE,
                payload: {message: error.response ? error.response.data && (error.response.data.message || error.response.data) : error.message}
            })
        }
        if (callback) {
            const response = error.response || {status: -1, data: {}}
            callback(dispatch, response.status, (response.data && response.data.message) || error.message);
        }
        performFinal();
    });
};

export const apiHttpPost = (url, requestBody = {}) => params => genericHttpApiAction('post', url, requestBody, params);

export const apiHttpPut = (url, requestBody) => params => genericHttpApiAction('put', url, requestBody, params);

export const apiHttpDelete = (url) => params => genericHttpApiAction('delete', url, null, params);

export const apiHttpGet = (url) => params => genericHttpApiAction('get', url, null, params);