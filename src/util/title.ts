import {APP_NAME} from "../common/constants";

export const setPageTitle = (title: string) => {
    document.title = title + ' | ' + APP_NAME
}