
export const urlWithParams = (url: string, params: {[key: string]: string}) => {
    const urlPrefix = !url || url.startsWith('/') ? 'http://127.0.0.1' : '';
    const fullUrl = urlPrefix + url;
    const result = new URL(fullUrl);
    Object.entries(params).forEach(([k, v]) => result.searchParams.set(k, v));
    return result.toString().replace(urlPrefix, '');
}

export const currentUrlWithParam = (key: string, value: string) => {
    const url = new URL(window.location.href);
    url.searchParams.set(key, value);
    return url.toString().replace(url.origin, "");
}

export const getUrlParams = (...keys: string[]) => {
    const result: {[key: string]: string} = {};
    const url = new URL(window.location.href);
    let keySet = new Set<string>();
    keys.forEach(k => keySet.add(k));
    url.searchParams.forEach( (v, k) => {
        if (keySet.has(k)) {
            result[k] = v;
        }
    });
    return result
}