import React from 'react';
import {connect} from "react-redux";
import {setPageTitle} from "../util/title";
import {Link} from "react-router-dom";
import {getUrlParams, urlWithParams} from "../util/url";
import {JsonQueryStatus, resetJson, selectJson} from "../redux/jsonQuery";
import "prismjs/themes/prism-tomorrow.css"
import "prismjs/components/prism-core.min"
import "prismjs/components/prism-json.min"
import "prismjs/plugins/line-numbers/prism-line-numbers.css"
import "prismjs/plugins/line-numbers/prism-line-numbers.min"
import Loader from "../chunks/Loader";
import {DEFAULT_FILE_MASK, DEFAULT_JSONPATH} from "../common/constants";

interface State {
    files: string,
    query: string
}

type Props = DispatchProps & StateProps

class Results extends React.PureComponent<Props, State> {
    constructor(props: Props, context: any) {
        super(props, context);
        this.state = {
            files: '',
            query: ''
        };
    }

    componentDidMount() {
        setPageTitle('Query result');

        let {files, query} = getUrlParams('files', 'query')
        files = files === undefined ? DEFAULT_FILE_MASK : files;
        query = query === undefined ? DEFAULT_JSONPATH : query;

        this.setState({files, query});

        this.props.selectJson(files, query);
    }

    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
        (window as any).Prism.highlightAll();
    }

    componentWillUnmount() {
        this.props.resetJson()
    }

    render() {
        const {files, query} = this.state;
        const backLink = urlWithParams("/", {files, query});

        const renderedResponse = () => {
            const entries = Object.entries(this.props.data);
            if (entries.length === 0) {
                return <p>The response has no matching data for this file selector and query path.</p>
            }
            return entries.map((([filename, json]) => <div className="row" key={filename}>
                <div className="col-xl-12">
                    <h2>{filename}</h2>
                </div>
                <div className="col-xl-12">
                    <pre className="language-json line-numbers"><code>{JSON.stringify(json, null, 2)}</code></pre>
                </div>
            </div>))
        };

        let content: any
        switch (this.props.status) {
            case JsonQueryStatus.PENDING:
                content = <Loader/>
                break;
            case JsonQueryStatus.LOADED:
                content = renderedResponse()
                break;
            case JsonQueryStatus.FAILED:
                content = <p>JSON query failed. Please, try different parameters, or try again if you sure.</p>
                break;
        }

        return <div className="row">
            <div className="col-xl-12">
                <Link className="btn btn-lg btn-block btn-light text-black-50" to={backLink} role="button">&lt;- Back to
                    search</Link>
                <hr/>
                <h1 className="pb-3">Results for <b>SELECT {this.state.query} FROM {this.state.files}</b>:</h1>
                {content}
            </div>
        </div>;
    }
}

interface DispatchProps {
    selectJson: (files: string, query: string) => void,
    resetJson: () => void,
}

const mapDispatch = {
    selectJson,
    resetJson
}

interface StateProps {
    status: JsonQueryStatus,
    data: { [filename: string]: any }
}

const mapState = (state: any) => ({
    status: state.jsonQuery.status,
    data: state.jsonQuery.data,
})

export default connect<StateProps, DispatchProps>(mapState, mapDispatch)(Results);