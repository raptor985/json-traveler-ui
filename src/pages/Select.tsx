import React from 'react';
import {connect} from "react-redux";
import {setPageTitle} from "../util/title";
import {Link} from "react-router-dom";
import {currentUrlWithParam, getUrlParams, urlWithParams} from "../util/url";
import {RouteComponentProps, withRouter} from "react-router";
import {DEFAULT_FILE_MASK, DEFAULT_JSONPATH} from "../common/constants";

interface State {
    files: string,
    query: string
}

type Props = RouteComponentProps

class Select extends React.PureComponent<Props, State> {
    constructor(props: Props, context: any) {
        super(props, context);
        this.state = {
            files: '',
            query: ''
        };
    }

    componentDidMount() {
        setPageTitle('Data selection');

        let {files, query} = getUrlParams('files', 'query')
        files = files === undefined ? DEFAULT_FILE_MASK : files;
        query = query === undefined ? DEFAULT_JSONPATH : query;

        this.setState({files, query})
    }

    render() {
        const {files, query} = this.state;
        const selectionLink = urlWithParams("/results", {files, query})

        return <div className="row">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-6 col-md-8 pt-2 pb-2 pt-md-3 pb-md-3 pt-lg-5 pb-lg-5">
                        <h1>New search</h1>
                        <hr />
                        File mask:
                        <div className="input-group input-group-lg mb-3">
                            <input type="text" className="form-control" placeholder="Filename wildcard. For multiple selection use *."
                                   aria-describedby="button-addon-1"
                                   value={this.state.files}
                                   onChange={e => {
                                       const filename = e.target.value;
                                       this.setState({files: filename}, () => {
                                           this.props.history.push(currentUrlWithParam('files', filename));
                                       });
                                   }}
                            />
                        </div>
                        Query:
                        <div className="input-group input-group-lg mb-3">
                            <input type="text" className="form-control" placeholder="JSONPath query"
                                   aria-describedby="button-addon-2"
                                   value={this.state.query}
                                   onChange={e => {
                                       const query = e.target.value;
                                       this.setState({query}, () => {
                                           this.props.history.push(currentUrlWithParam('query', query));
                                       })
                                   }}
                            />
                        </div>
                        { files && query ?
                            <Link className="btn btn-lg btn-block btn-primary" to={selectionLink} role="button">Search</Link> :
                            <p className="text-center">Please, enter the query and file mask</p>
                        }
                        <a className="btn btn-lg btn-block btn-light text-black-50" href={"/"} role="button">Reset defaults</a>
                    </div>
                </div>
            </div>
        </div>;
    }
}

export default connect()(withRouter(Select));