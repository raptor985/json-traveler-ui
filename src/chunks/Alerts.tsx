import React from 'react';
import {connect} from "react-redux";
import {AlertsState, closeAlert} from "../redux/alerts";

type Props = DispatchProps & StateProps

class Results extends React.PureComponent<Props> {

    render() {
        const entries = Object.entries(this.props.alerts);

        if (entries.length === 0) {
            return null;
        }

        return <div className="row">
            <div className="col-xl-12">
                {entries.map(([id, alert]) => <div className="alert alert-danger alert-dismissible fade show" role="alert" key={id}>
                    {alert.message}
                    <button type="button" className="btn btn-link btn-close" data-bs-dismiss="alert"
                            aria-label="Close" onClick={() => this.props.closeAlert(id)}>
                        &#10005;
                    </button>
                </div>)}
            </div>
        </div>;
    }
}

interface DispatchProps {
    closeAlert: (id: string) => void,
}

const mapDispatch = {
    closeAlert
}

interface StateProps {
    alerts: AlertsState
}

const mapState = (state: any) => ({
    alerts: state.alerts,
})

export default connect<StateProps, DispatchProps>(mapState, mapDispatch)(Results);