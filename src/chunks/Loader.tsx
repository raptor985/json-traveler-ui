import React from "react";

type Props = LoaderProps

class Loader extends React.PureComponent<Props, {}> {
    render() {
        return (<div className={`text-center p-3 loader ${this.props.className || ''}`}>Loading...</div>);
    }
}

interface LoaderProps extends React.HTMLAttributes<HTMLElement>{
    className?: string
}

export default Loader;